import java.io.File
import java.util.*

//================================
var Matchdata = ""
val scangame = Scanner(System.`in`)
//================================

/*
    Nombre: Orlando David
    Apellidos: Chirinos Cisneros
    Módulo: M03
    Fecha de inicio: 11/01/2023
    Fecha de finalización: 12/02/2023.
 */

//Función "Random": https://www.develou.com/random-en-kotlin/
//Función ".any"  : https://www.develou.com/funcion-any-en-kotlin/

fun decidenewgame(){

    /*
        Deja al usuario varias opciones a elegir, al introducir una de éstas se ejecuta la función correspondiente, caso
        contrario se ejecuta nuevamente ésta misma función. Se pudo haber hecho con un simple WHEN, a corregir.

        L: Logros
        N: Nuevo juego
        H: Historial
        Q: Salir (Q de QUIT)
     */

    println("Seleccione a continuación la sección a la que desea acceder:\n\n" +
            "${Kolor.foreground("Logros",Color.LIGHT_CYAN)}: ¡Pulse [L] para visualizar aquellos logros que ha conseguido a lo largo del juego!(En desarrollo...)\n" +
            "${Kolor.foreground("Nuevo juego",Color.LIGHT_MAGENTA)}: Pulse [N] para iniciar una nueva partida.\n" +
            "${Kolor.foreground("Historial",Color.LIGHT_YELLOW)}: Pulse [H] para mostrar el historial de partidas.\n" +
            "${Kolor.foreground("Salir", Color.RED)}: Pulse [Q] para salir del programa.\n"
    )

    //==============================
    //          SELECCIÓN
    //==============================
    print("¿Cómo desea proceder?: ")
    //===================================

    var choice = scan.next().toLowerCase()

    //===================================
    if (choice == "l"){
        achievements()
    }
    else if (choice == "n"){
        println(Kolor.foreground("====================================================================",Color.WHITE))
        print("¿Está seguro de querer empezar una nueva partida? [Y] / [N]: ")
        //==============================
        val newgame = scan.next().toLowerCase()
        //===============================
        if (newgame == "n"){
            println("\n${Kolor.foreground("Volviendo a pantalla de selección.",Color.LIGHT_YELLOW)}")
            println(Kolor.foreground("====================================================================",Color.WHITE))
            decidenewgame()
        }
        else if(newgame == "y"){
            println(Kolor.foreground("====================================================================",Color.WHITE))
            useradd()
            game(languagedata())
        }

        else{
            println(Kolor.foreground("====================================================================",Color.WHITE))
            println(Kolor.foreground("Instrucción introducida no admisible", Color.LIGHT_YELLOW))
            println(Kolor.foreground("====================================================================",Color.WHITE))
            decidenewgame()
        }
    }

    else if(choice == "q"){
        println(Kolor.foreground("====================================================================",Color.WHITE))
        print("${Kolor.foreground("¿Está seguro de querer abandonar?", Color.LIGHT_YELLOW)} [Y] / [N]: ")
        choice = scan.next().toLowerCase()
        when (choice){
            "y" -> {println()}
            "n" -> {
                println(Kolor.foreground("====================================================================",Color.WHITE))
                decidenewgame()
            }
            else -> {
                println(Kolor.foreground("Instrucción introducida no admisible", Color.YELLOW))
                decidenewgame()}
        }
    }
    else if (choice == "h"){
        history()
    }

    else{
        println(Kolor.foreground("Instrucción introducida no admisible.", Color.LIGHT_RED))
        println(Kolor.foreground("====================================================================",Color.WHITE))
        decidenewgame()
    }
}
fun replay(){

    /*
        Esta función le permite al usuario decidir si quiere jugar una nueva partida después de haber perdido o ganado.
        En caso de no querer jugar más se debe pulsar N, el jugador volverá al menú principal.
        En caso de querer continuar, se debe pulsar Y para iniciar una nueva partida (Esto guarda tu nombre de usuario).
        Si se introduce una opción no válida se le indicará de ésto al usuario y se ejecuta la función nuevamente.
     */

    print("¿Le gustaría iniciar una nueva partida? [Y] / [N]: ")
    val replay = scan.next().toLowerCase()

    if (replay == "n") {
        println("\n${Kolor.foreground("Entendido, volviendo al menú principal ", Color.LIGHT_YELLOW)}...")
        println()
        main()
    }
    else if(replay == "y"){
        println("\n¡Fantástico! ${Kolor.foreground("Empezando nueva partida", Color.LIGHT_YELLOW)}...")
        println(Kolor.foreground("====================================================================",Color.WHITE))
        game(languagedata())
    }
    else{
        println(Kolor.foreground("Instrucción introducida no admisible.", Color.LIGHT_RED))
        println(Kolor.foreground("====================================================================",Color.WHITE))
        replay()
    }
}
fun game(lang: Int){

    /*
        Esta función se encarga del juego, tiene variables muy importantes:

        lang: Ésta variable tiene su origen de la selección de idioma. Puede tener dos valores:
              1: Español
              2: Inglés

        lifes: Inicia como 6, cada vez que el usuario se equivoque perderá una vida (lifes--).

        counter_intword: Inicia como 0, esto es para contar cuántas veces el usuario ha introducido una palabra
                         con más (o menos) de 5 caracteres. Sólo sirve para una pequeña broma en la cual Mr.Wordle se
                         enoja y reduce las vidas del jugador a 0. De haberse implementado el sistema de logros ésto
                         activaría su respectivo logro.

        wincondition: Inicia como null, ésta función permite que el bucle se mantenga en ejecución.
                      Si es true: Se rompe el bucle, se registra en Matchdata y se considera victoria.
                      Si es false: Se rompe el bucle, se registra en Matchdata y se considera derrota.
                      Si es null: Se mantiene el bucle.

        wordsdatabase: Es el archivo donde se almacenan las palabras, dependiendo del idioma seleccionado tendrá el
                       valor del archivo de palabras en inglés o en español.

        Matchdata: Datos de la partida, ordenado en: Nombre, vidas, palabra, idioma, condición de victoria.

     */
    //==============================
    var lifes = 6
    var counter_intword = 0
    var wincondition :Boolean? = null
    var wordsdatabase = File("")

    when (lang){
        1 -> wordsdatabase = File("./src/main/DATAWORDS/palabras.txt")
        2 -> wordsdatabase = File("./src/main/DATAWORDS/englishwords.txt")
    }

    val words: List<String> = wordsdatabase.readLines()

    //================================
    val generatedword = words.random().toString().toLowerCase()
    println(Kolor.foreground("====================================================================",Color.WHITE))
    println(Kolor.background(Kolor.foreground("La palabra ha sido generada.",Color.BLACK),Color.YELLOW))
    println(Kolor.foreground("====================================================================",Color.WHITE))
    //================================

    //==========================================================

    while (wincondition == null){
        when (counter_intword){
            1,2 -> println("\nTamaño de palabra no válido. ¡La palabra introducida ${Kolor.foreground("ha de ser de 5 caracteres",Color.RED)}!\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            3 -> println("\nNuevamente, la palabra introducida ${Kolor.foreground("ha de ser de 5 caracteres",Color.LIGHT_RED)}!\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            4 -> println("\nSupongo que sabrás leer, porque si no, no estarías aquí, la palabra introducida ${Kolor.foreground("ha de ser de 5 caracteres",Color.LIGHT_RED)}!\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            5 -> println("\nYa sabes ${Kolor.foreground("de cuantos caracteres",Color.LIGHT_RED)} debe ser la palabra.\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            6 -> println("\nÚltima advertencia. ${Kolor.foreground("5 CARACTERES",Color.LIGHT_RED)}\n" +
                    Kolor.foreground("====================================================================",Color.WHITE))
            7 -> {
                println(Kolor.foreground("\n¿Querías lograr algo, no? Felicidades, lo has logrado.", Color.LIGHT_RED))
                lifes = 0
                wincondition = false
            }
        }
        if (counter_intword < 7){
            //=============================
            println()
            var verifyword = ""
            print("Introduzca palabra: ")
            //=============================
            var intchar = scangame.nextLine().toLowerCase()
            verifyword += intchar

            if (verifyword.lastIndex != 4){
                counter_intword++}

            else if (verifyword == generatedword){

                wincondition = true
                println(Kolor.background(Kolor.foreground(verifyword.toUpperCase(),Color.BLACK),Color.GREEN))}

            else{
                counter_intword = 0
                lifes--

                if (verifyword.lastIndex != 4){
                    counter_intword++}

                else {
                    println()
                    for (i in 0..4){
                        if (verifyword[i] == generatedword[i]){
                            print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.GREEN))
                        }
                        else {
                            when (verifyword[i]){
                                generatedword[0] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                generatedword[1] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                generatedword[2] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                generatedword[3] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                generatedword[4] -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.DARK_GRAY))
                                else -> print(Kolor.background(Kolor.foreground(verifyword[i].toString().toUpperCase(),Color.BLACK),Color.RED))
                            }
                        }
                    }
                }
                println()
                when (lifes){
                    5,4 -> println("\n¡Te has equivocado! Vidas restantes: ${Kolor.foreground(lifes.toString(),Color.GREEN)}\n" +
                            Kolor.foreground("====================================================================",Color.WHITE))
                    3,2 -> println("\n¡Cuidado! Vidas restantes: ${Kolor.foreground(lifes.toString(),Color.LIGHT_YELLOW)}\n" +
                            Kolor.foreground("====================================================================",Color.WHITE))
                    1 -> println("\n¡Último intento! ¡Tienes sólo ${Kolor.foreground(lifes.toString(),Color.RED)} vida restante!\n" +
                            Kolor.foreground("====================================================================",Color.WHITE))
                    0 -> wincondition = false
                }

            }
        }
    }

    when (wincondition){
        true -> println("\n¡Felicidades, ${Kolor.foreground("HAS GANADO", Color.LIGHT_GREEN)}!")
        false -> println("\n¡Qué mala suerte, ${Kolor.foreground("HAS PERDIDO", Color.RED)}! La palabra correcta era: ${Kolor.foreground(generatedword.toString().toUpperCase(),Color.MAGENTA)} ")
    }

    if (counter_intword >= 7){println(Kolor.foreground("Suerte a la próxima, crack.",Color.RED))}

    println(Kolor.foreground("\n====================================================================",Color.WHITE))

    Matchdata = "$intname, $lifes, $generatedword, $langchoice, $wincondition\n"
    historyadd()
    replay()


    //==========================================================
}
fun achievements(){

    /*
        Ésta función es una pantalla con los distintos logros del juego, aunque de momento, al no estar implementada la
        mecánica de logros, es un elemento decorativo.
     */

    //=================WARNING=================
    println(Kolor.foreground("====================================================================",Color.WHITE))
    println("\n${Kolor.background(Kolor.foreground("This section is under construction, no achievements implemented.",Color.BLACK),Color.RED)}\n")
    println(Kolor.foreground("====================================================================",Color.WHITE))
    //=========================================
    println("${Kolor.foreground("Nuevo por aquí",Color.CYAN)}: Inicia sesión y juega tu primera partida de WORDLE.\n" +
            "${Kolor.foreground("¡Qué mala suerte!", Color.CYAN)}: Has perdido tu primera partida de WORDLE.\n" +
            "${Kolor.foreground("¡Victoria!", Color.CYAN)}: Has ganado tu primera partida de WORDLE.\n" +
            "${Kolor.foreground("Sólo es mala suerte", Color.LIGHT_CYAN)}: Has perdido 5 partidas de WORDLE seguidas.\n" +
            "${Kolor.foreground("Skill issue", Color.LIGHT_RED)}: Has perdido 10 partidas de WORDLE seguidas.\n" +
            "${Kolor.foreground("¡En racha!", Color.LIGHT_CYAN)}: Has ganado 5 partidas de WORDLE seguidas.\n" +
            "${Kolor.foreground("GG EZ", Color.LIGHT_YELLOW)}: Has ganado 10 partidas de WORDLE seguidas.\n" +
            "${Kolor.foreground("Buscando el chiste...", Color.RED)}: Has molestado a Mr.Wordle.")
    println(Kolor.foreground("====================================================================",Color.WHITE))
    println("\n${Kolor.background(Kolor.foreground("Going back to selection screen...",Color.BLACK),Color.RED)}\n")
    println(Kolor.foreground("====================================================================",Color.WHITE))
    decidenewgame()
}
fun main() {

    //Menú principal, ejecuta la función decidenewgame.

    println("===========================================================================================================================")

    println("Bienvenido a Wordle! A continuación, se explicará en qué consiste el juego: \n \nSe generará una palabra " +
            "aleatoria de 5 caracteres. \nDeberá introducir, por lo tanto, la palabra generada." +
            "\nDispone de ${Kolor.foreground("6", Color.RED)} vidas para cumplir éste objetivo.\n" +
            "El juego finaliza con usted como ${Kolor.foreground("ganador",Color.LIGHT_GREEN)} si cumple dicho objetivo" +
            ", caso contrario ${Kolor.foreground("será fin de partida.",Color.RED)}\n")
    println(
        "El fondo tendrá un color u otro donde:\n ${
            Kolor.background(
                Kolor.foreground("Verde", Color.BLACK),
                Color.GREEN
            )
        }: Representa que la palabra y su posicion son correctos.\n ${
            Kolor.background(
                Kolor.foreground("Gris ", Color.BLACK),
                Color.DARK_GRAY
            )
        }: Representa que la letra existe pero no en la posición correcta.\n ${
            Kolor.background(
                Kolor.foreground("Rojo ", Color.BLACK),
                Color.RED
            
            )
        }: Representa que la letra no existe."
    )
    println("===========================================================================================================================")
    println("")
    decidenewgame()

}



