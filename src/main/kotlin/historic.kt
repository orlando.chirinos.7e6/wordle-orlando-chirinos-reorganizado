import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match
import java.util.Scanner
import java.io.File
val historyFile = File("./src/main/HISTORIC/matches.txt")
fun historyadd(){

    // Añade un espacio de historial.

    historyFile.appendText(Matchdata)
    Matchdata = ""
}
fun history(){
    //===================================================
    /*
        Esta función se divide en dos partes, verifica si el archivo de partidas jugadas (matches.txt) está vacío o no:
        En caso de estarlo le indica al usuario que no hay partidas guardadas.
        En caso de no estarlo le muestra al usuario el historial de partidas jugadas.
     */
    //===================================================

    println("${Kolor.foreground("====================================================================",Color.WHITE)}\n\n" +
            Kolor.foreground("Historial de partidas: ",Color.LIGHT_YELLOW))

    val matchlist = arrayListOf<String>()
    val matchesplayed = historyFile.forEachLine { matchlist.add(it) }
    //======================
    //=====VERIFICACIÓN======
    //======================
    //ARCHIVO VACÍO:
    //======================

    if (matchlist.toString() == "[]"){
        println("${Kolor.foreground("====================================================================",Color.WHITE)}\n" +
                "${Kolor.background(Kolor.foreground("No existe ninguna partida guardada en nuestra base de datos . . .",Color.BLACK),Color.LIGHT_YELLOW)}\n" +
                Kolor.foreground("====================================================================",Color.WHITE)
        )
    }
    //======================
    //ARCHIVO CON PARTIDAS GUARDADAS:
    //======================

    else{
        for (i in matchlist.lastIndex downTo 0){
            val name = matchlist[i].split(",")[0]
            val lifes = matchlist[i].split(",")[1]
            val generatedword = matchlist[i].split(",")[2]
            val langchoice = matchlist[i].split(",")[3]
            val wincondition = matchlist[i].split(",")[4]
            println()
            println("${Kolor.foreground("====================================================================",Color.WHITE)}\n" +
                    "${Kolor.foreground("Usuario: $name",Color.LIGHT_MAGENTA)}\n" +
                    "${Kolor.foreground("Vidas restantes: $lifes", Color.LIGHT_CYAN)}\n" +
                    Kolor.foreground("Palabra: ${generatedword.toUpperCase()}", Color.LIGHT_YELLOW)
            )
            when (langchoice){
                " es" -> println(Kolor.foreground("Idioma: ESPAÑOL", Color.LIGHT_BLUE))
                " en" -> println(Kolor.foreground("Idioma: INGLÉS", Color.LIGHT_BLUE))
            }

            if (wincondition == " true"){
                print("${Kolor.foreground("Resultado de partida: ",Color.WHITE)}${Kolor.foreground("VICTORIA",Color.LIGHT_GREEN)}")
                println(Kolor.foreground("\n====================================================================",Color.WHITE))
            }
            else {
                print("${Kolor.foreground("Resultado de partida: ",Color.WHITE)}${Kolor.foreground("DERROTA",Color.LIGHT_RED)}")
                println(Kolor.foreground("\n====================================================================",Color.WHITE))
            }
        }
    }
    //======================
    //PRINT AL USUARIO
    //======================
    print(Kolor.foreground("Introduzca cualquier caracter y presione [ENTER] para volver a la pantalla de selección: ",Color.LIGHT_YELLOW))
    scan.next()
    println(Kolor.foreground("====================================================================",Color.WHITE))
    decidenewgame()
    //======================

}
