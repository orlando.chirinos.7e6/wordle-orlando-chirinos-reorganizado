import java.util.Scanner
//================================
val scan = Scanner(System.`in`)
var langchoice = ""
var intname = ""
//================================
fun languagedata():Int{

    /*
    Pide al usuario que introduzca el idioma en el que desea jugar. Devuelve un valor tipo INT (1 o 2) para que se use
    en la función game (el juego).
     */

    println("Idiomas disponibles (aplicable únicamente en gameplay, no en interfaz): \n" +
            "${Kolor.foreground("ESP - Español (Predeterminado) [ES]",Color.LIGHT_YELLOW)}\n" +
            Kolor.foreground("ENG - Inglés [EN]",Color.BLUE)
            )

    print("Seleccione uno de los idiomas indicados: ")

    langchoice = scan.next().toLowerCase()
    var langchoice_taken = 0

    when (langchoice){
        "es" -> langchoice_taken = 1
        "en" -> langchoice_taken = 2
        else -> {
            println("${Kolor.foreground("====================================================================",Color.WHITE)}\n" +
                    "${Kolor.foreground("Instrucción introducida no admisible", Color.YELLOW)}\n" +
                    "${Kolor.foreground("====================================================================",Color.WHITE)}\n")
            langchoice_taken = languagedata()
        }
    }
    return langchoice_taken
}

fun useradd(){

    // El usuario introduce su nombre de jugador. Se añade el valor a la variable intname.

    print(Kolor.foreground("Introduzca su nombre: ", Color.LIGHT_MAGENTA))
    intname = scan.next().toUpperCase()
    println()
    Matchdata = ""
    Matchdata += intname

}

